#include "Tracker.h"

Tracker::Tracker(const float ** ranges) : ranges_p(ranges)
{
	//default.
	this->threshold = 20;


}

/*this constructor recives HSV image*/

Tracker::Tracker(const cv::Mat & image, const float ** ranges) : Tracker(ranges)
{
	cv::Mat cloneImage = image.clone(), hist;
	int histSize[] = { 16, 16 };
	cv::calcHist(&cloneImage, 1, channels, cv::Mat(), hist, 2, histSize, ranges_p);
	cv::normalize(hist, hist, 0, 255, cv::NORM_MINMAX, -1, cv::Mat());
	histogram = hist.clone();
}

inline Tracker::~Tracker()
{

}

/*This function recives image and search window and retuns meanshift translation vector.
This is only one meanshift iteration.*/

inline cv::Point Tracker::processMeanshift(const cv::Mat & image) {
	if (image.cols == 0 || image.rows == 0)
		return cv::Point(0, 0);
	cv::Point translation, centroid, currentCenter;
	cv::Mat imageClone = cv::Mat(image);
	cv::calcBackProject(&imageClone, 1, channels, cv::Mat(this->getHistogram()), this->probDist, ranges_p, 1, true);
	moments = cv::moments(this->probDist);

	//centroid location.
	centroid = cv::Point(moments.m10 / moments.m00, moments.m01 / moments.m00);
	currentCenter = cv::Point((probDist.cols) / 2.0, (probDist.rows) / 2.0);

	translation = centroid - currentCenter;

	//usporavanje pomicanja search windowa testirati i odrediti konstantu.
	translation /= 1.0;

	return translation;
}

/*This function recives image and search window and retuns camshift expansion vector.*/

inline cv::Point Tracker::processCamshiftWindowExpanson(const cv::Mat & image) {
	if (image.cols == 0 || image.rows == 0)
		return cv::Point(0, 0);
	cv::Point expansion;

	cv::Mat imageClone = cv::Mat(image);
	cv::calcBackProject(&imageClone, 1, channels, cv::Mat(this->getHistogram()), this->probDist, ranges_p, 1, true);
	moments = cv::moments(this->probDist);

	float s = 2 * cv::sqrt(moments.m00 / 256);
	expansion = cv::Point(s, 1.2*s);
	return expansion;
}

/*Returns coordinates of object
image -  image in which we search object
start - starting point of rectangle (object window)
end - ending point of rectangle (object window)
*/

void Tracker::getNextPosition(const cv::Mat & image, cv::Point & start, cv::Point & end, double & angle)
{

	cv::Point translation, expansion, extraSize(10, 10), center;
	cv::Mat imageClone;
	int maxstepsexpansion = 5, meanshiftsteps = 20;

	do {
		do {
			imageClone = image.clone();
			cropImage(imageClone, start, end);
			translation = processMeanshift(imageClone);
			translation *= 1.0;
			processSearchRegionChange(start, end, translation, translation, image);

		} while (cv::norm(translation) > this->threshold && --meanshiftsteps);

		imageClone = image.clone();
		processSearchRegionChange(start, end, -extraSize, extraSize, image);
		cropImage(imageClone, start, end);
		expansion = processCamshiftWindowExpanson(imageClone);
		center = (end + start) / 2.0;
		start = end = center;
		processSearchRegionChange(start, end, -expansion / 2.0, expansion / 2.0, image);

	} while (cv::norm(expansion) > this->threshold && --maxstepsexpansion);

	double xc = moments.m10 / moments.m00;
	double yc = moments.m01 / moments.m00;

	angle = std::atan2(2 * (moments.m11 / moments.m00 - xc * yc),
		(moments.m20 / moments.m00 - xc * xc) - (moments.m02 / moments.m00 - yc * yc)) / 2;


}

cv::Mat Tracker::getProbDist() {
	return this->probDist;
}

cv::Mat Tracker::getHistogram() {
	return this->histogram;
}

inline void Tracker::processSearchRegionChange(cv::Point & start, cv::Point & end, const cv::Point & corner1, const cv::Point & corner2, const cv::Mat & image) {
	start += corner1;
	end += corner2;

	if (start.x >= image.cols)
		start.x = image.cols - 1;

	if (start.x < 0)
		start.x = 0;

	if (end.x >= image.cols)
		end.x = image.cols - 1;

	if (end.x < 0)
		end.x = 0;

	if (start.y >= image.rows)
		start.y = image.rows - 1;

	if (start.y < 0)
		start.y = 0;

	if (end.y >= image.rows)
		end.y = image.rows - 1;

	if (end.y < 0)
		end.y = 0;

}

inline void Tracker::cropImage(cv::Mat & image, const cv::Point & start, const cv::Point & end) {
	//dodati neki veci odrez.

	if (start.x < 0 || start.x >= image.cols || start.y < 0 || start.y >= image.rows
		|| end.x < 0 || end.x >= image.cols || end.y < 0 || end.y >= image.rows)
		return;

	cv::Rect myROI(start, end);
	image = image(myROI);
}
