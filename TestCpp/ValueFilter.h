#ifndef _VALUE_FILTER_H_
#define _VALUE_FILTER_H_

#include "IFilter.h"

class ValueFilter : public IFilter{
public:
    ValueFilter(const int lowerBound, const int upperBound){
        this->lowerBound = lowerBound;
        this->upperBound = upperBound;
    }
    ~ValueFilter(){

    }
    virtual static cv::Mat process(const cv::Mat &hsvImage){
        cv::Mat returnImage = hsvImage;
        cv::inRange(hsvImage, cv::Scalar(0, 0, lowerBound), cv::Scalar(255, 255, upperBound), returnImage);
        return returnImage;
    }
private:
    int lowerBound, upperBound;
}


#endif


