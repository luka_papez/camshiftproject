#pragma once
class IFilter
{
public:
	Filter();
	~Filter();
    virtual static cv::Mat process(const cv::Mat &image) = 0;
private:
};

