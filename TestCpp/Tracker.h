#ifndef _TRACKER_H_
#define _TRACKER_H_

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"


class Tracker
{
public:
	Tracker(const float **ranges);
	/*this constructor recives HSV image*/
	Tracker(const cv::Mat &image, const float **ranges);
	virtual ~Tracker();
	/*This function recives image and search window and retuns meanshift translation vector.
	  This is only one meanshift iteration.*/
	cv::Point processMeanshift(const cv::Mat &image);
	/*This function recives image and search window and retuns camshift expansion vector.*/
	cv::Point processCamshiftWindowExpanson(const cv::Mat &image);
	/*Returns coordinates of object
	  image -  image in which we search object
	  start - starting point of rectangle (object window)
	  end - ending point of rectangle (object window)
	*/
	void getNextPosition(const cv::Mat &image, cv::Point &start, cv::Point &end, double &angle);
	cv::Mat getProbDist();
	cv::Mat getHistogram();
private:
	void processSearchRegionChange(cv::Point &start, cv::Point &end, const cv::Point &corner1, const cv::Point &corner2, const cv::Mat &image);
	void cropImage(cv::Mat &image, const cv::Point &start, const cv::Point &end);
	cv::Moments moments;
	cv::Mat probDist;
	cv::Mat histogram;
	cv::Point searchWindowStart, searchWindowEnd;
	float threshold;
	int channels[2] = { 0, 1 };
	const float **ranges_p;
};

#endif
