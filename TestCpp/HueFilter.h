#ifndef _HUE_FILTER_H_
#define _HUE_FILTER_H_

#include "IFilter.h"

class HueFilter : public IFilter{
public:
    HueFilter(const int lowerBound, const int upperBound){
        this->lowerBound = lowerBound;
        this->upperBound = upperBound;
    }
    ~HueFilter(){

    }
    virtual static cv::Mat process(const cv::Mat &hsvImage){
        cv::Mat returnImage = hsvImage;
        cv::inRange(hsvImage, cv::Scalar(lowerBound, 0, 0), cv::Scalar(upperBound, 255, 255), returnImage);
        return returnImage;
    }
private:
    int lowerBound, upperBound;
}

#endif



