#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <vector>

#include "Tracker.h"

using namespace cv;
using namespace std;

// Raspon vrijednosti koji ulazi u racunanje histograma.
// hranges bi uvijek trebao biti { 0, 180 }
// sranges moze varirati ovisno o osvjetljenju
float hranges[] = { 0, 180 };
float sranges[] = { 20, 255 };
const float* ranges[] = { hranges, sranges };

// globalne tocke selektiranog pravokutnika na ekranu
cv::Point rectStart, rectEnd;

// nuzno za selektiranje
bool searchWindowSelected = false;
bool pressed;

// funkcija koja obavlja interakciju misem
void CallBackFunc(int event, int x, int y, int flags, void* userdata) {

	if (event == cv::EVENT_LBUTTONDOWN) {
		rectStart = cv::Point(x, y);
		searchWindowSelected = false;
		pressed = true;
	}
	else if (event == cv::EVENT_LBUTTONUP) {
		rectEnd = cv::Point(x, y);
		searchWindowSelected = true;
		pressed = false;
	}
	else if (event == cv::EVENT_MOUSEMOVE) {
		if(pressed)
			rectEnd = cv::Point(x, y);
	}
	else if (event == cv::EVENT_MBUTTONDOWN){
        // moguce implementirati funkcionalnost desnog klika misa
	}
}

// reze sliku unutar danog pravokutnika, pazeci na dimenzije
void CropImage(Mat &src, Point p1, Point p2, Mat &res) {
	if (p1.x < 0 || p1.x >= src.cols || p1.y < 0 || p1.y >= src.rows ||
        p2.x < 0 || p2.x >= src.cols || p2.y < 0 || p2.y >= src.rows){
        return;
    }

	Rect myROI(p1, p2);
	res = src(myROI);
	if (res.rows == 0 || res.cols == 0)
		res = src;
}

// ulazna funkcija u program
/**
*   Ulaz u program je putanja do videa na kojem se zeli vrsiti pracenje objekata.
*   Ukoliko nije dan niti jedan argument program podrazumjeva da korisnik
*   zeli pratiti s kamere te otvara default video device za pracenje.
*   Objekt koji se prati se oznacuje misem.
*   Pritiskom broja na tipkovnici se mice tracker pod tim brojem.
*   Video se pauzira i pokrece pritiskom na slovo p.
*/
int main(int argc, char** argv) {

	VideoCapture cap;

    // ako argumenti nisu predani
    if(argc == 1){
        cap.open(0);
    }else{
    // inace otvori zadanu datoteku
        cap.open(argv[1]);
    }

    // za slucaj pogreske u otvaranju streama
	if (!cap.isOpened())
		return -1;

    // program podrzava najvise 9 trackera
    Tracker* tracker[9];
    for(int i = 0; i < 9; i++){
        tracker[i] = NULL;
    }
    pair<cv::Point, cv::Point>* trackerPoints[9];
    for(int i = 0; i < 9; i++){
        trackerPoints[i] = NULL;
    }
    // image drzi ucitani frame, HSV je taj frame pretvoren u HSV zapis
	Mat image, HSV;
	Point firstLineStart, firstLineEnd, secondLineStart, secondLineEnd, center;
	Tracker *lastTracker = NULL;

    // angle je nagib izracunati nagib lica, angle2 je angle + 90 stupnjeva
	double angle, angle2, pihalf = 3.14159265359 / 2;

	namedWindow("Camshift", 1);
	setMouseCallback("Camshift", CallBackFunc, NULL);

    // brojimo koliko trackera ima
    int trackerCount = 0;

    // boje raznoraznih trackera
    Scalar colors[] = {
        Scalar(0, 0, 255), Scalar(0, 255, 0), Scalar(255, 0, 0),
        Scalar(0, 255, 255), Scalar(255, 255, 0), Scalar(255, 0, 255),
        Scalar(255, 125, 0), Scalar(255, 255, 255), Scalar(0, 255, 125),
        Scalar(0, 0, 0)
    };

	char buttonPressed;
	while (1) {
        // pretvaranje slike u zeljeni oblik
		cap >> image;
		cv::cvtColor(image, HSV, CV_BGR2HSV);
        // ukoliko smo oznacili pravokutnik
		if (searchWindowSelected) {
			if (lastTracker == NULL && trackerCount < 9) {
				Mat imageROI;
				CropImage(HSV, rectStart, rectEnd, imageROI);
				lastTracker = new Tracker(imageROI, ranges);
                // stavljanje tracker u prvo slobodno mjesto u polju ako nema mjesta ne stavlja se nista
                tracker[trackerCount] = new Tracker(imageROI, ranges);
                // stavljanje tocaka u prvi slobodan
                trackerPoints[trackerCount] = new pair<Point, Point>(rectStart, rectEnd);

				//crtanje histograma
				lastTracker->getNextPosition(HSV, rectStart, rectEnd, angle);

				int w = 400; int h = 400;
				int bin_w = cvRound((double)w / 180);
				Mat histogram = Mat::zeros(w, h, CV_8UC3);

				Mat histogramClone = lastTracker->getHistogram().clone();

				for (int i = 0; i < 180; i++)
				{
					rectangle(histogram, Point(i*bin_w, h), Point((i + 1)*bin_w, h - cvRound(histogramClone.at<float>(i)*h / 255.0)), Scalar(255, 255, 255), -1);
				}

				imshow("Histogram" + std::to_string(trackerCount + 1), histogram);

                // dodali smo tracker
                trackerCount++;
			}

		}
		else {
			lastTracker = NULL;
			rectangle(image, rectStart, rectEnd, colors[trackerCount], 1);
		}
		for(int i = 0; i < trackerCount; i++){

            tracker[i]->getNextPosition(HSV, trackerPoints[i]->first, trackerPoints[i]->second, angle);

			//crtanje distribucije
			imshow("PD" + std::to_string(i + 1), tracker[i]->getProbDist());

            // crtanje pravokutnika
            rectangle(image, trackerPoints[i]->first, trackerPoints[i]->second, colors[i], 1);

            // crtanje linije
            center = (trackerPoints[i]->second + trackerPoints[i]->first) / 2.0;
            // definiramo područje u kojem linije moraju bit
            Rect rect(trackerPoints[i]->first, trackerPoints[i]->second);
            // broj mora biti dovoljno velik da crte uvijek ispunjavaju pravokutnik
            double diagonal = 1000;
			double angleCos = std::cos(angle);
			double angleSin = std::sin(angle);
			// velika os lica
            firstLineStart = cv::Point( (-angleCos * diagonal), (-angleSin * diagonal)) + center;
            firstLineEnd = cv::Point( (angleCos * diagonal), (angleSin * diagonal)) + center;
            // odreze tocke tako da ostanu unutar pravokutnika
            clipLine(rect, firstLineStart, firstLineEnd);
            line(image, firstLineStart, firstLineEnd, colors[i]);

            // mala os lica
            angle2 = angle + pihalf;
			double angle2cos = std::cos(angle2);
			double angle2sin = std::sin(angle2);
            secondLineStart = cv::Point( (-angle2cos * diagonal), (-angle2sin * diagonal)) + center;
            secondLineEnd = cv::Point( (angle2cos * diagonal), (angle2sin * diagonal)) + center;
            clipLine(rect, secondLineStart, secondLineEnd);
            line(image, secondLineStart, secondLineEnd, colors[i]);
		}

        // prikazi rezultat
        imshow("Camshift", image);

        // klikom na tipkovnicu mice se tracker
		buttonPressed = cvWaitKey(1);

		if(buttonPressed == '\27'){
            // Escape je izlazak iz programa
			return 1;
		}else if(isdigit(buttonPressed)){
            int trackerIndex = buttonPressed - '0' - 1;
            // ako je dobar index pritisnut
            if(trackerIndex >= 0 && tracker[trackerIndex] != NULL){
                // micanje otvorenih prozora
                for(int i = 0; i < trackerCount; i++){
                    destroyWindow("PD" + std::to_string(i + 1));
                    destroyWindow("Histogram" + std::to_string(i + 1));
                }

                // brisanje podataka
                delete tracker[trackerIndex];
                tracker[trackerIndex] = NULL;
                delete trackerPoints[trackerIndex];
                trackerPoints[trackerIndex] = NULL;

                trackerCount--;
                // preslozi trackere tako da polje bude uvijek zauzeto od nule
                for(int i = 0; i < 9; i++){
                    if(tracker[i] == NULL){
                        for(int j = i; j < 9; j++){
                            if(tracker[j] != NULL){
                                tracker[i] = tracker[j];
                                tracker[j] = NULL;
                                trackerPoints[i] = trackerPoints[j];
                                trackerPoints[j] = NULL;
                                break;
                            }
                        }
                    }
                }

                // ponovno crtanje prozora
                for(int i = 0; i < trackerCount; i++){
                    int w = 400; int h = 400;
                    int bin_w = cvRound((double)w / 180);
                    Mat histogram = Mat::zeros(w, h, CV_8UC3);

                    Mat histogramClone = tracker[i]->getHistogram().clone();

                    for (int i = 0; i < 180; i++)
                    {
                        rectangle(histogram, Point(i*bin_w, h), Point((i + 1)*bin_w, h - cvRound(histogramClone.at<float>(i)*h / 255.0)), Scalar(255, 255, 255), -1);
                    }

                    imshow("Histogram" + std::to_string(i + 1), histogram);
                    imshow("PD" + std::to_string(i + 1), tracker[i]->getProbDist());
                }

            }
            else{
				// inace nemoj nista napravit
            }
		}else if(buttonPressed == 'p'){
            // ako je pritisnut p pauziraj video
            while(true){
                // crtaj kvadrat
                cv::Mat stillImage = image.clone();
                rectangle(stillImage, rectStart, rectEnd, Scalar(0, 255, 0), 1);
                imshow("Camshift", stillImage);
                buttonPressed = cvWaitKey(1);
                // ako treba nastaviti dodaj tracker i updateaj histogram
                if(buttonPressed == 'p'){
                    Mat imageROI;
                    CropImage(HSV, rectStart, rectEnd, imageROI);
                    lastTracker = new Tracker(imageROI, ranges);
                    // stavljanje tracker u prvo slobodno mjesto u polju ako nema mjesta ne stavlja se nista
                    tracker[trackerCount] = new Tracker(imageROI, ranges);
                    // stavljanje tocaka u prvi slobodan
                    trackerPoints[trackerCount] = new pair<Point, Point>(rectStart, rectEnd);
                    //crtanje histograma
                    lastTracker->getNextPosition(HSV, rectStart, rectEnd, angle);

                    int w = 400; int h = 400;
                    int bin_w = cvRound((double)w / 180);
                    Mat histogram = Mat::zeros(w, h, CV_8UC3);

                    Mat histogramClone = lastTracker->getHistogram().clone();

                    for (int i = 0; i < 180; i++)
                    {
                        rectangle(histogram, Point(i*bin_w, h), Point((i + 1)*bin_w, h - cvRound(histogramClone.at<float>(i)*h / 255.0)), Scalar(255, 255, 255), -1);
                    }

                    imshow("Histogram" + std::to_string(trackerCount + 1), histogram);

                    // dodali smo tracker
                    trackerCount++;
                    break;
                }
            }
		}
	}
}













