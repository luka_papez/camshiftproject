#ifndef _SATURATION_FILTER_H_
#define _SATURATION_FILTER_H_

#include "IFilter.h"

class SaturationFilter : public IFilter{
public:
    SaturationFilter(const int lowerBound, const int upperBound){
        this->lowerBound = lowerBound;
        this->upperBound = upperBound;
    }
    ~SaturationFilter(){

    }
    virtual static cv::Mat process(const cv::Mat &hsvImage){
        cv::Mat returnImage = hsvImage;
        cv::inRange(hsvImage, cv::Scalar(0, lowerBound, 0), cv::Scalar(255, upperBound, 255), returnImage);
        return returnImage;
    }
private:
    int lowerBound, upperBound;
}

#endif


